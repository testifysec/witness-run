# TestifySec CI/CD Components

This repo contains Gitlab CI/CD components to make generating and using [in-toto](https://in-toto.io) attestations in your pipelines easy.

## witness-run

Installs [Witness](https://github.com/in-toto/witness) into the provided image and observes the provided command with Witness, creating a signed attestation about the execution of the command.
By default the witness-run component will use the public [Sigstore](https://sigstore.dev) instance to get a short-lived key to sign the attestation.

The provided image must have `curl` installed, or be installed in a `before_script`.

A simple use of the component may look like:

```yaml
include:
  - component: gitlab.com/testifysec/cicd-components/witness-run@{VERSION}
    inputs:
      stage: test
      step: hello-world
      image: registry.gitlab.com/testifysec/cicd-components/test-image:latest
      command: echo "hello world"
```

The above will create a job named `hello-world` in the `test` stage of your pipeline that runs `echo "hello world"` and outputs a signed attestation to `stdout`.

To save the attestation to disk and store it as a job artifact, one could provide the `outfile` input to the component, then re-define the job with the `artifacts` block added:

```yaml
include:
  - component: gitlab.com/testifysec/cicd-components/witness-run@{VERSION}
    inputs:
      stage: test
      step: component-test
      image: registry.gitlab.com/testifysec/cicd-components/test-image:latest
      command: echo "hello world"
      outfile: attestation.json

component-test:
  artifacts:
    paths:
      - attestation.json
```

A similar approach can be used to setup a Docker daemon to build an image:

```yaml
include:
  - component: gitlab.com/testifysec/cicd-components/witness-run@{VERSION}
    inputs:
      stage: build
      step: docker-build
      image: docker:24.0.5
      command: docker build -t test .
      outfile: attestation.json

docker-build:
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  services:
    - docker:24.0.5-dind
  before_script:
    - apk --no-cache add curl # witness-run component needs curl to install witness
  artifacts:
    paths:
      - attestation.json
```

| Input                                  | Default Value          | Description                                                                                                                                                                                                                                                                 |
| -------------------------------------- | ---------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `stage`                                |                        | Stage of the pipeline for the job                                                                                                                                                                                                                                           |
| `image`                                |                        | Image to use for the job                                                                                                                                                                                                                                                    |
| `witness-version`                      | 0.3.1                  | Version of Witness to use                                                                                                                                                                                                                                                   |
| `witness-download-url`                 |                        | Url to download witness from. This URL is expected to point to a tarball containing a witness binary. If unspecified, witness will attempt to be downloaded from public Github using the witness-version input. If specified, the witness-version variable will be ignored. |
| `witness-install-dir`                  | /usr/local/bin/        | Directory to install Witness into                                                                                                                                                                                                                                           |
| `skip-witness-install`                 |                        | Skips the install of Witness. This is useful if you already have Witness installed in the image your job uses. Witness must be in your image's PATH.                                                                                                                        |
| `command`                              |                        | Command to run with witness                                                                                                                                                                                                                                                 |
| `step`                                 |                        | Name of the step being run                                                                                                                                                                                                                                                  |
| `enable-fulcio`                        | true                   | Use keyless signing through Sigstore Fulcio to sign attestation                                                                                                                                                                                                             |
| `fulcio-id-token-audience`             | sigstore               | Audience to use for the GitLab ID Token that will be used to get a certificate from Sigstore Fulcio                                                                                                                                                                         |
| `archivista-server`                    |                        | URL of the Archivista server to store or retrieve attestations (default "<https://archivista.testifysec.io>")                                                                                                                                                               |
| `attestations`                         | environment,git,gitlab | Attestations to record ('product' and 'material' are always recorded) (default [environment,git,gitlab])                                                                                                                                                                    |
| `attestor-maven-pom-path`              |                        | The path to the Project Object Model (POM) XML file used for task being attested (default "pom.xml"). (default "pom.xml")                                                                                                                                                   |
| `attestor-product-exclude-glob`        |                        | Pattern to use when recording products. Files that match this pattern will be excluded as subjects on the attestation.                                                                                                                                                      |
| `attestor-product-include-glob`        |                        | Pattern to use when recording products. Files that match this pattern will be included as subjects on the attestation. (default "\*")                                                                                                                                       |
| `enable-archivista`                    |                        | Use Archivista to store or retrieve attestations                                                                                                                                                                                                                            |
| `hashes`                               |                        | Hashes selected for digest calculation. Defaults to SHA256 (default [sha256])                                                                                                                                                                                               |
| `outfile`                              |                        | File to which to write signed data. Defaults to stdout                                                                                                                                                                                                                      |
| `signer-file-cert-path`                |                        | Path to the file containing the certificate for the private key                                                                                                                                                                                                             |
| `signer-file-intermediate-paths`       |                        | Paths to files containing intermediates required to establish trust of the signer's certificate to a root                                                                                                                                                                   |
| `signer-file-key-path`                 |                        | Path to the file containing the private key                                                                                                                                                                                                                                 |
| `signer-fulcio-oidc-client-id`         |                        | OIDC client ID to use for authentication                                                                                                                                                                                                                                    |
| `signer-fulcio-oidc-issuer`            |                        | OIDC issuer to use for authentication                                                                                                                                                                                                                                       |
| `signer-fulcio-oidc-redirect-url`      |                        | OIDC redirect URL (Optional). The default oidc-redirect-url is '<http://localhost:0/auth/callback>'.                                                                                                                                                                        |
| `signer-fulcio-url`                    |                        | Fulcio address to sign with                                                                                                                                                                                                                                                 |
| `signer-fulcio-token`                  |                        | Raw token string to use for authentication to fulcio (cannot be used in conjunction with --fulcio-token-path)                                                                                                                                                               |
| `signer-fulcio-token-path`             |                        | Path to the file containing a raw token to use for authentication to fulcio (cannot be used in conjunction with --fulcio-token)                                                                                                                                             |
| `signer-kms-aws-config-file`           |                        | The shared configuration file to use with the AWS KMS signer provider                                                                                                                                                                                                       |
| `signer-kms-aws-credentials-file`      |                        | The shared credentials file to use with the AWS KMS signer provider                                                                                                                                                                                                         |
| `signer-kms-aws-insecure-skip-verify`  |                        | Skip verification of the server's certificate chain and host name                                                                                                                                                                                                           |
| `signer-kms-aws-profile`               |                        | The shared configuration profile to use with the AWS KMS signer provider                                                                                                                                                                                                    |
| `signer-kms-aws-remote-verify`         |                        | verify signature using AWS KMS remote verification. If false, the public key will be pulled from AWS KMS and verification will take place locally (default true)                                                                                                            |
| `signer-kms-gcp-credentials-file`      |                        | The credentials file to use with the GCP KMS signer provider                                                                                                                                                                                                                |
| `signer-kms-hashType`                  |                        | The hash type to use for signing (default "sha256")                                                                                                                                                                                                                         |
| `signer-kms-keyVersion`                |                        | The key version to use for signing                                                                                                                                                                                                                                          |
| `signer-kms-ref`                       |                        | The KMS Reference URI to use for connecting to the KMS service                                                                                                                                                                                                              |
| `signer-spiffe-socket-path`            |                        | Path to the SPIFFE Workload API Socket                                                                                                                                                                                                                                      |
| `signer-vault-altnames`                |                        | Alt names to use for the generated certificate. All alt names must be allowed by the vault role policy                                                                                                                                                                      |
| `signer-vault-commonname`              |                        | Common name to use for the generated certificate. Must be allowed by the vault role policy                                                                                                                                                                                  |
| `signer-vault-namespace`               |                        | Vault namespace to use                                                                                                                                                                                                                                                      |
| `signer-vault-pki-secrets-engine-path` |                        | Path to the Vault PKI Secrets Engine to use (default "pki")                                                                                                                                                                                                                 |
| `signer-vault-role`                    |                        | Name of the Vault role to generate the certificate for                                                                                                                                                                                                                      |
| `signer-vault-token`                   |                        | Token to use to connect to Vault                                                                                                                                                                                                                                            |
| `signer-vault-ttl`                     |                        | Time to live for the generated certificate. Defaults to the vault role policy's configured TTL if not provided                                                                                                                                                              |
| `signer-vault-url`                     |                        | Base url of the Vault instance to connect to                                                                                                                                                                                                                                |
| `timestamp-servers`                    |                        | Timestamp Authority Servers to use when signing envelope                                                                                                                                                                                                                    |
| `trace`                                |                        | Enable tracing for the command                                                                                                                                                                                                                                              |
| `workingdir`                           |                        | Directory from which commands will run                                                                                                                                                                                                                                      |
| `config`                               |                        | Path to the witness config file (default ".witness.yaml")                                                                                                                                                                                                                   |
| `log-level`                            |                        | Level of logging to output (debug, info, warn, error) (default "info")                                                                                                                                                                                                      |
