#!/bin/bash

options=$(./witness run --help 2>&1| grep -E '\-\-signer-fulcio[a-zA-Z-]*' -o | sed -e "s/^--//")
echo $options

echo "if [ ! -z \"\$[[ inputs.enable-fulcio ]]\" ]; then"

while read option; do
    cat << EOF
    ${option//-/_}="\$[[ inputs.$option ]]"
    if [ -z "\$${option//-/_}" ]; then
        ${option//-/_}="<REPLACE WITH DEFAULT>"
    fi

    WITNESS_ARGS="\$WITNESS_ARGS --$option=\$${option//-/_}"

EOF
done<<<$options

echo "fi"