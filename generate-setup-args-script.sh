#!/bin/bash

options=$(./witness run --help 2>&1| grep -E '\-\-[a-zA-Z-]*' -o | grep -v -E "help|*fulcio*" | sed -e "s/^--//")

while read option; do
    cat << EOF
if [ ! -z "\$[[ inputs.$option ]]" ]; then
    WITNESS_ARGS="\$WITNESS_ARGS --$option=\$[[ inputs.$option ]]"
fi

EOF
done<<<$options
